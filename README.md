# Installation

- Clone project

- Change to the eZadravec branch

- Navigate to the project folder and execute the command "php artisan serve"

# API

## get token
- method: post
- url: /token
- params: {"email": "teste@teste.com", "senha": "123mudar"} 

## delete token
- method delete
- url: /{token}/token
- params: {"token"}

## get all customers
- method: get
- url: /{token}/customers

## get customer
- method: get
- url: /{token}/customer/{id}

## insert customer
- method: post
- url: /{token}/customer
- params: {"name", "email", "phone", "status", "cnpj"}

## update customer
- url: /{token}/customer
- method: put
- params: {"customer", ("name", "email", "phone", "status", "cnpj")}

## delete customer
- method: delete
- url: /{token}/customer
- params: {"customer"}