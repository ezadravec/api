<?php

namespace App\Utils;

use Illuminate\Http\Request;
use Validator;

class Response
{
	private $message = '';
	private $status = 'success';
	private $data = [];
	private $exception = false;

	public static function __callStatic($name, $params)
	{
		$class = new self();

		return call_user_func_array([$class, $name], $params);
	}

	public function __call($name, $params)
	{
		return call_user_func_array([$this, $name], $params);
	}

	private function setStatus($status = 'success')
	{
		$this->status = $status;

		return $this;
	}

	private function setData($data = [], $clear = false)
	{
		$this->data = ($clear) ? $data : array_merge($this->data, $data);

		return $this;
	}

	private function getResponse($response = 200)
	{
		return response()->json([
			'status' 	=> $this->status,
			'message' 	=> $this->message,
			'data'		=> $this->data,
		], $response, [], JSON_PRETTY_PRINT);
	}
}