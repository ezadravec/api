<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Utils\Response as IResponse;
use Validator;
use App\Models\User as UserModel;
use App\Models\Token as TokenModel;

class Login extends Controller
{
    public function login(Request $request)
    {
    	if ($request->headers->get('Content-Type') != 'application/json')
            return IResponse::setStatus('error')->setData(['Json is only allowed'])->getResponse(400);

    	$validator = Validator::make($request->all(), [
    		'email' 	=> 'required|email',
    		'password'	=> 'required',
		]);

		if ($validator->fails()) {
            $erro = $validator->errors()->toArray();
            return IResponse::setStatus('error')->setData($erro)->getResponse(400);
        }

       	$user = UserModel::where(['email' => $request->input('email'), 'password' => md5($request->input('password'))])
       		->first();

       	if (!$user)
            return IResponse::setStatus('error')->setData(['Authentication failed'])->getResponse(400);

        $token = new TokenModel();

        $token->token = md5(uniqid(rand(), true));
        $token->id_user = $user->id;
        $token->expiration_date = date('Y-m-d H:i:s', strtotime('+1 hour'));
        $token->save();

        return IResponse::setStatus('success')->setData(['token' => $token->token])->getResponse(400);
    }

    public function logout(Request $request)
    {
        if ($request->headers->get('Content-Type') != 'application/json')
            return IResponse::setStatus('error')->setData(['Json is only allowed'])->getResponse(400);
            
        $validator = Validator::make($request->all(), [
            'token'     => 'required',
        ]);

        if ($validator->fails()) {
            $erro = $validator->errors()->toArray();
            return IResponse::setStatus('error')->setData($erro)->getResponse(400);
        }

        TokenModel::where('token', $request->input('token'))->delete();
        
        return IResponse::setStatus('success')->setData(['Session closed'])->getResponse(400);
    }
}
