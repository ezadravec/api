<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Utils\Response as IResponse;
use App\Models\Customer as CustomerModel;
use Validator;

class Customer extends Controller
{
    public function getAll(Request $request)
    {
    	$customers = CustomerModel::get()->toArray();

        return IResponse::setStatus('success')->setData($customers)->getResponse(200);

    }

    public function get(Request $request, $token, $id = null)
    {
    	if ($id === null)
            return IResponse::setStatus('error')->setData([ "id" => [ "The id param is required." ]])->getResponse(400);

    	$customer = CustomerModel::where('id', $id)->first();

    	if (empty($customer))
            return IResponse::setStatus('error')->setData(['Customer not found'])->getResponse(400);

        return IResponse::setStatus('success')->setData($customer->toArray())->getResponse(200);	
    }

    public function update(Request $request)
    {
    	if ($request->headers->get('Content-Type') != 'application/json')
            return IResponse::setStatus('error')->setData(['Json is only allowed'])->getResponse(400);

        $validator = Validator::make($request->all(), [
    		'customer' 	=> 'required|integer',
    		'email' 	=> 'max:50|email',
    		'phone' 	=> 'max:13',
    		'cnpj' 		=> 'max:18',
    		'status' 	=> 'boolean',
		]);

		if ($validator->fails()) {
            $erro = $validator->errors()->toArray();
            return IResponse::setStatus('error')->setData($erro)->getResponse(400);
        }

    	$customer = CustomerModel::where('id', $request->input('customer'))->first();

    	if (empty($customer))
            return IResponse::setStatus('error')->setData(['Customer not found'])->getResponse(400);

    	$request->input('name') 	&& $customer->name = $request->input('name');
    	$request->input('email') 	&& $customer->email = $request->input('email');
    	$request->input('phone') 	&& $customer->phone = $request->input('phone');
    	$request->input('cnpj') 	&& $customer->cnpj = $request->input('cnpj');
    	$request->input('status') 	&& $customer->status = $request->input('status');

    	$customer->save();
           
        return IResponse::setStatus('success')->setData($customer->toArray())->getResponse(400);
    }

    public function insert(Request $request)
    {
    	if ($request->headers->get('Content-Type') != 'application/json')
            return IResponse::setStatus('error')->setData(['Json is only allowed'])->getResponse(400);

        $validator = Validator::make($request->all(), [
    		'email' 	=> 'required|max:50|email',
    		'phone' 	=> 'required|max:13',
    		'cnpj' 		=> 'required|max:18',
    		'status' 	=> 'required|boolean',
		]);

		if ($validator->fails()) {
            $erro = $validator->errors()->toArray();
            return IResponse::setStatus('error')->setData($erro)->getResponse(400);
        }

        $customer = new CustomerModel();

        $customer->name = $request->input('name');
    	$customer->email = $request->input('email');
    	$customer->phone = $request->input('phone');
    	$customer->cnpj = $request->input('cnpj');
    	$customer->status = $request->input('status');

    	$customer->save();
           
        return IResponse::setStatus('success')->setData($customer->toArray())->getResponse(400);
    }

    public function delete(Request $request)
    {
    	if ($request->headers->get('Content-Type') != 'application/json')
            return IResponse::setStatus('error')->setData(['Json is only allowed'])->getResponse(400);

        $validator = Validator::make($request->all(), [
    		'customer' 	=> 'required|integer',
		]);

		if ($validator->fails()) {
            $erro = $validator->errors()->toArray();
            return IResponse::setStatus('error')->setData($erro)->getResponse(400);
        }

    	if ($customer = CustomerModel::where('id', $request->input('customer'))->delete())
     	   return IResponse::setStatus('success')->setData(['Customer deleted'])->getResponse(400);
     	else
            return IResponse::setStatus('error')->setData(['Customer not found'])->getResponse(400);
    }
}
