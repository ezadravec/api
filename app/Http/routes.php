<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::post('/token', 'Login@login');

Route::group(['prefix' => '{token}', 'middleware' => 'auth'], function () {
	Route::get('customers', 'Customer@getAll');
	
	Route::get('customer/{id?}', 'Customer@get');
	Route::put('customer', 'Customer@update');
	Route::post('customer', 'Customer@insert');
	Route::delete('customer', 'Customer@delete');

	Route::delete('token', 'Login@logout');
});