<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Utils\Response as IResponse;
use App\Models\Token as TokenModel;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $params = (object) $request->route()->parameters();

        $token = TokenModel::where('token', $params->token)->first();
        
        if (empty($token)) 
            return IResponse::setStatus('error')->setData(['not authorized'])->getResponse(401);

         // Verifica se a sessao expirou
        if (date('Y-m-d H:i:s') > date('Y-m-d H:i:s', strtotime($token->expiration_date))) {
            
            $token->delete();

            return IResponse::setStatus('error')->setData(['Token expired'])->getResponse(401);
        }
                
        $token->expiration_date = date('Y-m-d H:i:s', strtotime('+1 hour'));
        $token->save();
        
        return $next($request);
    }
}
